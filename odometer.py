def is_strictly_increasing(n: str) -> bool:
    return n == sorted(n)

def is_zero(n: str) -> bool:
    return 0 == min(n)

def next_reading(n: int) -> int:
    while(0):
        next_step = (n + 1) % 1000
        if is_strictly_increasing(n) and is_zero(n):
            return next_step


print(next_reading(123))

